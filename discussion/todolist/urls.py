from django.urls import path
# from . import views
from todolist.views import index, todoitem, register, change_password, login_view, logout_view, add_task, update_task, delete_task


# Adding a namespace to this urls.py help django distinguish this set of routes from other urls.py files in other packages
app_name = 'todolist'
urlpatterns = [
    path('', index, name = 'index'),

    # localhost:8000/todolist/1
    path('<int:todoitem_id>/', todoitem, name = 'viewtodoitem'),
    
    path('register/', register, name = "register"),

    path('change-password/', change_password, name = "change_password"),

    path('login/', login_view, name = "login"),

    path('logout/', logout_view, name = "logout"),

    path('add_task/', add_task, name = "add_task"),

    path('<int:todoitem_id>/edit', update_task, name = "update_task"),

    path('<int:todoitem_id>', delete_task, name = "delete_task")


]

















# urlpatterns = [
#     path('', views.index, name = 'index'),

#     # localhost:8000/todolist/1
#     path('<int:todoitem_id>/', views.todoitem, name = 'viewtodoitem'),
    
#     path('register/', views.register, name = "register"),

#     path('change-password/', views.change_password, name = "change_password"),

#     path('login/', views.login_view, name = "login"),

#     path('logout/', views.logout_view, name = "logout")

# ]